//
//  File.swift
//  iTunesMusicPlayer
//
//  Created by Anton  Tikhonov on 4/11/17.
//  Copyright © 2017 Anton  Tikhonov. All rights reserved.
//

import Foundation

// Data model for a track.
struct TrackDataModel {
  let trackID: Int
  let trackName: String
  let artistName: String
  let albumName: String?
  let previewTrackURL: URL
  let artworkURL: URL?
  let releaseDate: Date?
  let trackLengthSec: TimeInterval?  // can be useful for UI initialization

  init?(json: [String : Any]) {
    guard
      let trackID = json["trackId"] as? Int,
      let trackName = json["trackName"] as? String,
      let artistName = json["artistName"] as? String,
      let previewTrackURL = URL(string: json["previewUrl"] as? String ?? "")
    else {
      logError("[TrackDataModel] failed to parse")
      return nil
    }

    // Mandatory fields.
    self.trackID = trackID
    self.trackName = trackName
    self.artistName = artistName
    self.previewTrackURL = previewTrackURL

    // Optional fields.
    self.albumName = json["collectionName"] as? String
    self.artworkURL = URL(string: json["artworkUrl60"] as? String ?? "")
    self.releaseDate = DateFormatter.itunes.date(from: json["releaseDate"] as? String ?? "")
    if let trackLengthMs = json["trackTimeMillis"] as? Int {
      self.trackLengthSec = Double(trackLengthMs) / 1000.0
    } else {
      self.trackLengthSec = nil
    }
  }
}
