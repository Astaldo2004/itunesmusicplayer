//
//  TrackTableViewCell.swift
//  iTunesMusicPlayer
//
//  Created by Anton  Tikhonov on 5/11/17.
//  Copyright © 2017 Anton  Tikhonov. All rights reserved.
//

import UIKit

class TrackTableViewCell: UITableViewCell {

  var isPlaying = false {
    didSet {
      playingIndicatorImageView.isHidden = !isPlaying
    }
  }

  // MARK: - UI elements
  fileprivate var artworkImageViewSize = CGFloat(0)
  fileprivate lazy var artworkImageView: UIImageView = {
    let imageView = UIImageView().forAutolayout()
    imageView.contentMode = .scaleAspectFill

    // Setup shadow
    imageView.layer.shadowColor = UIColor.black.cgColor
    imageView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
    imageView.layer.shadowRadius = 1.0
    imageView.layer.shadowOpacity = 0.5
    imageView.layer.masksToBounds = false

    return imageView
  }()

  fileprivate lazy var trackNameLabel: UILabel = {
    let label = UILabel().forAutolayout()
    label.numberOfLines = 1
    label.font = UIFont.systemFont(ofSize: 16)
    return label
  }()

  fileprivate lazy var artistNameLabel: UILabel = {
    let label = UILabel().forAutolayout()
    label.numberOfLines = 1
    label.font = UIFont.boldSystemFont(ofSize: 14)
    label.textColor = UIColor.gray
    return label
  }()

  fileprivate lazy var albumNameLabel: UILabel = {
    let label = UILabel().forAutolayout()
    label.numberOfLines = 1
    label.font = UIFont.systemFont(ofSize: 12)
    label.textColor = UIColor.gray
    return label
  }()

  fileprivate lazy var playingIndicatorImageView: UIImageView = {
    let name = "774-sound-3-toolbar"
    guard let image = UIImage(named: name) else { fatalError("Missing image resource: \(name)") }

    let imageView = UIImageView().forAutolayout()
    imageView.image = image
    imageView.isHidden = true
    return imageView
  }()

  // MARK: - Life cycle
  override func didMoveToSuperview() {
    contentView.addSubview(artworkImageView)
    contentView.addSubview(trackNameLabel)
    contentView.addSubview(artistNameLabel)
    contentView.addSubview(albumNameLabel)
    contentView.addSubview(playingIndicatorImageView)

    artworkImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16).isActive = true
    artworkImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
    artworkImageView.widthAnchor.constraint(equalToConstant: artworkImageViewSize).isActive = true
    artworkImageView.heightAnchor.constraint(equalToConstant: artworkImageViewSize).isActive = true

    playingIndicatorImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
    playingIndicatorImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20).isActive = true

    trackNameLabel.leadingAnchor.constraint(equalTo: artworkImageView.trailingAnchor, constant: 16).isActive = true
    trackNameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16).isActive = true
    trackNameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -50).isActive = true

    artistNameLabel.leadingAnchor.constraint(equalTo: artworkImageView.trailingAnchor, constant: 16).isActive = true
    artistNameLabel.topAnchor.constraint(equalTo: trackNameLabel.bottomAnchor, constant: 2).isActive = true
    artistNameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -50).isActive = true

    albumNameLabel.leadingAnchor.constraint(equalTo: artworkImageView.trailingAnchor, constant: 16).isActive = true
    albumNameLabel.topAnchor.constraint(equalTo: artistNameLabel.bottomAnchor, constant: 2).isActive = true
    albumNameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -50).isActive = true

    let con = artworkImageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16)
    con.priority = .defaultLow
    con.isActive = true
  }

  // MARK: - Configuration
  func configure(track: String, artist: String, album: String, artworkURL: URL, artworkSize: Int, isPlaying: Bool, network: NetworkApi) {
    artworkImageViewSize = CGFloat(artworkSize)
    artworkImageView.image = nil // prepare for cell recycling
    trackNameLabel.text = track
    artistNameLabel.text = artist
    albumNameLabel.text = album

    self.isPlaying = isPlaying

    network.fetchArtwork(url: artworkURL) { [weak self] result in
      guard let sself = self else { return }

      onMainThread {
        switch result {
        case .success(let image):
          sself.artworkImageView.image = image
        case .failed(let error):
          logError("[TrackTableViewCell] can't load artwork from \(artworkURL): \(error?.quoted() ??? "")")
        }
      }
    }
  }

  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
}
