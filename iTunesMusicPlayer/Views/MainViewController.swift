//
//  MainViewController.swift
//  iTunesMusicPlayer
//
//  Created by Anton  Tikhonov on 4/11/17.
//  Copyright © 2017 Anton  Tikhonov. All rights reserved.
//

import UIKit

final class MainViewController: UIViewController {

  // MARK: - Assembly
  private lazy var network: NetworkApi = ItunesNetworkApi()
  private lazy var datasource: DataSource = ItunesDataSource(networkApi: network)
  private lazy var dateFormatter: DateFormatter = {
    let df = DateFormatter()
    df.dateFormat = "yyyy"  // display just the year of release
    return df
  }()
  fileprivate lazy var player: AudioPlayer = {
    let player = ItunesAudioPlayer()
    player.delegate = self
    return player
  }()

  private lazy var viewModel: ViewModel = {
    return MainViewModel(datasource: self.datasource, dateFormatter: self.dateFormatter)
  }()

  // MARK: - Child view contollers
  fileprivate lazy var trackListViewController: TrackListViewController = {
    return TrackListViewController(viewModel: self.viewModel, network: network, delegate: self)
  }()
  fileprivate lazy var searchViewController: SearchBarViewController = {
    return SearchBarViewController(viewModel: self.viewModel)
  }()
  fileprivate lazy var miniPlayerViewController: MiniPlayerViewController = {
    return MiniPlayerViewController(delegate: self)
  }()
  fileprivate lazy var fullSizePlayerViewController: FullSizePlayerViewController = {
    return FullSizePlayerViewController(network: self.network,
                                        artworkImageSize: viewModel.artworkSizeInPlayer,
                                        delegate: self)
  }()

  // MARK: - UI elements
  fileprivate lazy var navigationBar: UINavigationBar = UINavigationBar().forAutolayout()

  enum LayoutMode {
    case portrait
    case landscape

    static var current: LayoutMode {
      let frame = UIScreen.main.bounds
      return frame.width > frame.height ? .landscape : .portrait
    }

    var modelLayout: ViewLayout {
      switch self {
      case .portrait: return .tall
      case .landscape: return .wide
      }
    }

    static func fromModelLayout(_ modelLayout: ViewLayout) -> LayoutMode {
      switch modelLayout {
      case .tall: return .portrait
      case .wide: return .landscape
      }
    }
  }

  // MARK: Life cycle
  override func viewDidLoad() {
    super.viewDidLoad()

    addChildViewController(trackListViewController)
    addChildViewController(searchViewController)

    trackListViewController.view.translatesAutoresizingMaskIntoConstraints = false
    searchViewController.view.translatesAutoresizingMaskIntoConstraints = false

    view.addSubview(trackListViewController.view)
    view.addSubview(searchViewController.view)
    view.addSubview(navigationBar)

    navigationBar.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
    navigationBar.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
    navigationBar.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true

    trackListViewController.didMove(toParentViewController: self)
    searchViewController.didMove(toParentViewController: self)

    viewModel.attach(with: self)
    viewModel.layoutChanged(to: LayoutMode.current.modelLayout)
  }

  override var prefersStatusBarHidden: Bool {
    return false
  }

  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)

    coordinator.animate(alongsideTransition: nil) { context in
      // Rotation has finished
      self.viewModel.layoutChanged(to: LayoutMode.current.modelLayout)
    }
  }

  func layoutUI(for layoutMode: LayoutMode) {
    // Deactivate old constrains so that they don't clash with the new ones.
    view.constraints
      .filter {
        let firstView = $0.firstItem as? UIView
        let secondView = $0.secondItem as? UIView
        return firstView == searchViewController.view    ||
               secondView == searchViewController.view   ||
               firstView == trackListViewController.view ||
               secondView == trackListViewController.view
      }
      .forEach { $0.isActive = false }

    // Activate new constraints depending on layout mode.
    switch layoutMode {
    case .portrait:
      searchViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
      searchViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
      searchViewController.view.topAnchor.constraint(equalTo: navigationBar.bottomAnchor).isActive = true
      searchViewController.view.heightAnchor.constraint(equalToConstant: 40).isActive = true

      trackListViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
      trackListViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
      trackListViewController.view.topAnchor.constraint(equalTo: searchViewController.view.bottomAnchor).isActive = true
      trackListViewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    case .landscape:
      let halfWidth = view.bounds.width / 2.0
      trackListViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
      trackListViewController.view.topAnchor.constraint(equalTo: navigationBar.bottomAnchor).isActive = true
      trackListViewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
      trackListViewController.view.widthAnchor.constraint(equalToConstant: halfWidth).isActive = true

      searchViewController.view.leadingAnchor.constraint(equalTo: trackListViewController.view.trailingAnchor).isActive = true
      searchViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
      searchViewController.view.topAnchor.constraint(equalTo: navigationBar.bottomAnchor).isActive = true
      searchViewController.view.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
  }

  override func viewDidDisappear(_ animated: Bool) {
    viewModel.detach()
  }
}

// MARK: ViewModelView implementation
extension MainViewController: ViewModelView {

  func cancelSearch() {
    searchViewController.cancelSearch()
  }

  func selectTrack(at index: Int) {
    trackListViewController.setTrackWith(index: index, selected: true)
  }

  func unselectAllTracks() {
    trackListViewController.unselectAllTracks()
  }

  func setTrack(at index: Int, playing: Bool) {
    trackListViewController.setTrackWith(index: index, playing: playing)
  }

  func activateMiniPlayerWithTrack(at index: Int) {
    guard let trackModel = viewModel.track(at: index) else { return }
    miniPlayerViewController.load(track: trackModel)
    showMiniPlayer()
  }

  func deactivateMiniPlayer() {
    detachChild(viewController: miniPlayerViewController)
  }

  func activateFullSizePlayerWithTrack(at index: Int) {
    guard let trackModel = viewModel.track(at: index) else { return }
    fullSizePlayerViewController.load(track: trackModel)
    showFullSizePlayer()
  }

  func deactivateFullSizePlayer() {
    detachChild(viewController: fullSizePlayerViewController)
  }

  func setPlayButton(state: PlayerPlayButtonState) {
    miniPlayerViewController.setPlayButton(play: state == .play)
    fullSizePlayerViewController.setPlayButton(play: state == .play)
  }

  func playerLoad(url: URL) {
    player.loadTrack(at: url)
  }

  func playerPlay() {
    player.play()
  }

  func playerPause() {
    player.pause()
  }

  func reloadTracks() {
    trackListViewController.reloadTracks()
  }

  func changeLayout(to modelLayout: ViewLayout) {
    layoutUI(for: LayoutMode.fromModelLayout(modelLayout))
  }

  func display(error: String) {
    // TODO: needs implementation
  }
}

// MARK: TrackListViewDelegate implementation
extension MainViewController: TrackListViewDelegate {
  func didSetTrackAt(index: Int, selected: Bool) {
    if selected {
      viewModel.trackTapped(index: index)
    }
  }
}

// MARK: PlayerViewDelegate implementation
extension MainViewController: PlayerViewDelegate {
  func playButtonTapped() {
    viewModel.playButtonTapped()
  }
}

extension MainViewController {
  func showMiniPlayer() {
    attachChild(viewController: miniPlayerViewController) {
      miniPlayerViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
      miniPlayerViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
      miniPlayerViewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
      miniPlayerViewController.view.heightAnchor.constraint(equalToConstant: 100).isActive = true
    }
  }

  func showFullSizePlayer() {
    attachChild(viewController: fullSizePlayerViewController) {
      fullSizePlayerViewController.view.leadingAnchor.constraint(equalTo: trackListViewController.view.trailingAnchor).isActive = true
      fullSizePlayerViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
      fullSizePlayerViewController.view.topAnchor.constraint(equalTo: searchViewController.view.bottomAnchor).isActive = true
      fullSizePlayerViewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
  }
}

extension MainViewController: AudioPlayerDelegate {
  func startedPlaying() {
  }

  func finished() {
    viewModel.playerFinished()
  }

  func errorOccured(_ error: String?) {
  }
}
