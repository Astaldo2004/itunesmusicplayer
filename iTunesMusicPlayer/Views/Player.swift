//
//  Player.swift
//  iTunesMusicPlayer
//
//  Created by Anton  Tikhonov on 6/11/17.
//  Copyright © 2017 Anton  Tikhonov. All rights reserved.
//

import AVFoundation

protocol AudioPlayer {
  func loadTrack(at: URL)
  func play()
  func pause()
}

protocol AudioPlayerDelegate: AnyObject {
  func startedPlaying()
  func finished()
  func errorOccured(_ error: String?)
}

final class ItunesAudioPlayer: NSObject, AudioPlayer {

  fileprivate var player = AVPlayer()
  weak var delegate: AudioPlayerDelegate?

  deinit {
    NotificationCenter.default.removeObserver(self)
  }

  func loadTrack(at url: URL) {
    do {
      try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
      try AVAudioSession.sharedInstance().setActive(true)

      player = AVPlayer(url: url)
      NotificationCenter.default.addObserver(self,
                                             selector: #selector(playerFinishedPlaying),
                                             name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                             object: player.currentItem)
      player.play()

      delegate?.startedPlaying()
    } catch let error {
      delegate?.errorOccured(error.localizedDescription)
    }
  }

  func play() {
    guard player.status == .readyToPlay else { return }
    player.play()
  }

  func pause() {
    player.pause()
  }

  @objc func playerFinishedPlaying() {
    delegate?.finished()
  }
}

