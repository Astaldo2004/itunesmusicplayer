//
//  PlayerView.swift
//  iTunesMusicPlayer
//
//  Created by Anton  Tikhonov on 6/11/17.
//  Copyright © 2017 Anton  Tikhonov. All rights reserved.
//

import Foundation

protocol PlayerView: AnyObject {
  func load(track: TrackPresentationModel)
  func unloadTrack()
  func setPlayButton(enabled: Bool)
  func setPlayButton(play: Bool)
}

protocol PlayerViewDelegate: AnyObject {
  func playButtonTapped()
}
