//
//  FullSizePlayerViewController.swift
//  iTunesMusicPlayer
//
//  Created by Anton  Tikhonov on 6/11/17.
//  Copyright © 2017 Anton  Tikhonov. All rights reserved.
//

import UIKit

class FullSizePlayerViewController: UIViewController {

  // MARK: UI components
  fileprivate lazy var artworkImageView: UIImageView = {
    let imageView = UIImageView().forAutolayout()
    imageView.contentMode = .scaleAspectFill

    // Setup shadow
    imageView.layer.shadowColor = UIColor.black.cgColor
    imageView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
    imageView.layer.shadowRadius = 1.0
    imageView.layer.shadowOpacity = 0.5
    imageView.layer.masksToBounds = false

    return imageView
  }()

  fileprivate let buttonsTintColor = UIColor(red: 0.349, green: 0.639, blue: 0.976, alpha: 1)
  fileprivate lazy var playButton: PlayButton = {
    let button = PlayButton(frame: .zero, state: .play).forAutolayout()
    button.tintColor = buttonsTintColor
    button.addTarget(self, action: #selector(FullSizePlayerViewController.playButtonTapped), for: .touchUpInside)
    return button
  }()

  fileprivate weak var delegate: PlayerViewDelegate?
  fileprivate let network: NetworkApi
  fileprivate let artworkImageSize: Int

  // MARK: Life cycle
  init(network: NetworkApi, artworkImageSize: Int, delegate: PlayerViewDelegate) {
    self.network = network
    self.delegate = delegate
    self.artworkImageSize = artworkImageSize

    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    // UI setup
    view.translatesAutoresizingMaskIntoConstraints = false

    view.addSubview(artworkImageView)
    view.addSubview(playButton)

    artworkImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 20).isActive = true
    artworkImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    artworkImageView.widthAnchor.constraint(equalToConstant: CGFloat(artworkImageSize)).isActive = true
    artworkImageView.heightAnchor.constraint(equalToConstant: CGFloat(artworkImageSize)).isActive = true

    playButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    playButton.topAnchor.constraint(equalTo: artworkImageView.bottomAnchor, constant: 20).isActive = true
  }

  @objc fileprivate func playButtonTapped() {
    delegate?.playButtonTapped()
  }
}

extension FullSizePlayerViewController: PlayerView {
  func load(track: TrackPresentationModel) {
    network.fetchArtwork(url: track.artworkURL) { [weak self] result in
      guard let sself = self else { return }

      onMainThread {
        switch result {
        case .success(let image):
          sself.artworkImageView.image = image
        case .failed(let error):
          logError("[FullSizePlayerViewController] can't load artwork from \(track.artworkURL): \(error?.quoted() ??? "")")
        }
      }
    }
  }

  func unloadTrack() {
    artworkImageView.image = nil
  }

  func setPlayButton(enabled: Bool) {
    playButton.isEnabled = enabled
  }

  func setPlayButton(play: Bool) {
    playButton.buttonState = play ? .play : .pause
  }
}
