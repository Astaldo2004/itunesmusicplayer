//
//  MiniPlayerViewController.swift
//  iTunesMusicPlayer
//
//  Created by Anton  Tikhonov on 6/11/17.
//  Copyright © 2017 Anton  Tikhonov. All rights reserved.
//

import UIKit

class MiniPlayerViewController: UIViewController {
  // MARK: UI components
  fileprivate let kButtonsTintColor = UIColor(red: 0.349, green: 0.639, blue: 0.976, alpha: 1)
  fileprivate lazy var playButton: PlayButton = {
    let button = PlayButton(frame: .zero, state: .play).forAutolayout()
    button.tintColor = kButtonsTintColor
    button.addTarget(self, action: #selector(MiniPlayerViewController.playButtonTapped), for: .touchUpInside)
    return button
  }()
  fileprivate lazy var backgroundBlurView: UIVisualEffectView = {
    let blurEffect = UIBlurEffect(style: .dark)
    let view = UIVisualEffectView(effect: blurEffect).forAutolayout()
    return view
  }()

  weak var delegate: PlayerViewDelegate?

  // MARK: Life cycle
  init(delegate: PlayerViewDelegate) {
    self.delegate = delegate

    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    // UI setup
    view.translatesAutoresizingMaskIntoConstraints = false

    view.addSubview(backgroundBlurView)
    view.addSubview(playButton)

    backgroundBlurView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
    backgroundBlurView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    backgroundBlurView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
    backgroundBlurView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

    playButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    playButton.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
  }

  @objc fileprivate func playButtonTapped() {
    delegate?.playButtonTapped()
  }
}

extension MiniPlayerViewController: PlayerView {
  func load(track: TrackPresentationModel) {
    // No-op in mini player
  }

  func unloadTrack() {
    // No-op in mini player
  }

  func setPlayButton(enabled: Bool) {
    playButton.isEnabled = enabled
  }

  func setPlayButton(play: Bool) {
    playButton.buttonState = play ? .play : .pause
  }
}
