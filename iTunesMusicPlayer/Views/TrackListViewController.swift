//
//  TrackListViewController.swift
//  iTunesMusicPlayer
//
//  Created by Anton  Tikhonov on 5/11/17.
//  Copyright © 2017 Anton  Tikhonov. All rights reserved.
//

import UIKit

protocol TrackListView: AnyObject {
  func reloadTracks()
  func setTrackWith(index: Int, selected: Bool)
  func unselectAllTracks()
  func setTrackWith(index: Int, playing: Bool)
}

protocol TrackListViewDelegate: AnyObject {
  func didSetTrackAt(index: Int, selected: Bool)
}

class TrackListViewController: UIViewController {

  fileprivate var viewModel: ViewModel
  fileprivate var network: NetworkApi

  weak var delegate: TrackListViewDelegate?

  fileprivate let kTrackCellReuseID = "TrackCell"
  fileprivate lazy var tableView: UITableView = self.createTableView()

  init(viewModel: ViewModel, network: NetworkApi, delegate: TrackListViewDelegate) {
    self.viewModel = viewModel
    self.network = network
    self.delegate = delegate

    super.init(nibName: nil, bundle: nil)
  }

  // MARK: - Life cycle
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    buildUI()
  }
}

extension TrackListViewController: TrackListView {
  func reloadTracks() {
    tableView.reloadData()
  }

  func setTrackWith(index: Int, selected: Bool) {
    let indexPath = IndexPath(row: index, section: 0)
    if selected {
      tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
    } else {
      tableView.deselectRow(at: indexPath, animated: true)
    }
  }

  func unselectAllTracks() {
    for indexPath in tableView.indexPathsForSelectedRows ?? [] {
      tableView.deselectRow(at: indexPath, animated: false)
    }
  }

  func setTrackWith(index: Int, playing: Bool) {
    let indexPath = IndexPath(row: index, section: 0)
    guard let cell = tableView.cellForRow(at: indexPath) as? TrackTableViewCell else { return }
    cell.isPlaying = playing
  }
}

// MARK: - Private helpers
extension TrackListViewController {
  func buildUI() {
    view.addSubview(tableView)

    tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
    tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
    tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
  }

  func createTableView() -> UITableView {
    let tableView = UITableView().forAutolayout()
    tableView.dataSource = self
    tableView.delegate = self
    tableView.register(TrackTableViewCell.self, forCellReuseIdentifier: kTrackCellReuseID)
    tableView.estimatedRowHeight = 100
    tableView.rowHeight = UITableViewAutomaticDimension

    // Make separator lines to go all the way accross.
    tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    // Disable extra separator lines at the bottom of the table.
    tableView.tableFooterView = UIView()

    return tableView
  }
}

// MARK: - Table view data source
extension TrackListViewController: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.tracksCount
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard
      let cell = tableView.dequeueReusableCell(withIdentifier: kTrackCellReuseID, for: indexPath) as? TrackTableViewCell,
      let model = viewModel.track(at: indexPath.row)
    else {
        fatalError()
    }
    configure(cell: cell, with: model)
    return cell
  }

  fileprivate func configure(cell: TrackTableViewCell, with model: TrackPresentationModel) {
    cell.configure(track: model.trackName,
                   artist: model.artistName,
                   album: model.albumName,
                   artworkURL: model.artworkURL,
                   artworkSize: viewModel.artworkSizeInList,
                   isPlaying: model.isPlaying,
                   network: network)
  }
}

extension TrackListViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    delegate?.didSetTrackAt(index: indexPath.row, selected: true)
  }

  func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    delegate?.didSetTrackAt(index: indexPath.row, selected: false)
  }
}
