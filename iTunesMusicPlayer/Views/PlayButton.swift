//
//  PlayButton.swift
//  iTunesMusicPlayer
//
//  Created by Anton  Tikhonov on 7/11/17.
//  Copyright © 2017 Anton  Tikhonov. All rights reserved.
//

import UIKit

class PlayButton: UIButton {

  enum PlayButtonState {
    case play
    case pause

    var imageName: String {
      switch self {
      case .play: return "1241-play"
      case .pause: return "1242-pause"
      }
    }
    var selectedImageName: String {
      switch self {
      case .play: return "1241-play-selected"
      case .pause: return "1242-pause-selected"
      }
    }
  }

  var buttonState: PlayButtonState {
    didSet {
      setupButton(for: buttonState)
    }
  }

  init(frame: CGRect, state: PlayButtonState) {
    self.buttonState = state
    super.init(frame: frame)

    setupButton(for: buttonState)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  fileprivate func setupButton(for state: PlayButtonState) {
    let imageName = buttonState.imageName
    let selectedImageName = buttonState.selectedImageName

    setImage(UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate), for: .normal)
    setImage(UIImage(named: selectedImageName)?.withRenderingMode(.alwaysTemplate), for: .highlighted)
  }
}
