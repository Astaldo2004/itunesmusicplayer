//
//  SearchBarViewController.swift
//  iTunesMusicPlayer
//
//  Created by Anton  Tikhonov on 5/11/17.
//  Copyright © 2017 Anton  Tikhonov. All rights reserved.
//

import UIKit

protocol SearchView {
  func cancelSearch()
  func setSearchText(_ text: String)
}

class SearchBarViewController: UIViewController {

  fileprivate var viewModel: ViewModel

  // MARK: - UI elements
  fileprivate lazy var searchTextField: UITextField = {
    let textField = UITextField().forAutolayout()
    textField.placeholder = "Search by artist"
    textField.backgroundColor = UIColor.groupTableViewBackground
    textField.clearButtonMode = .whileEditing
    textField.clearsOnBeginEditing = true
    textField.delegate = self
    textField.returnKeyType = .search
    textField.enablesReturnKeyAutomatically = true
    return textField
  }()

  // MARK: - Life cycle
  init(viewModel: ViewModel) {
    self.viewModel = viewModel

    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    view.addSubview(searchTextField)

    searchTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
    searchTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
    searchTextField.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -8).isActive = true
  }

  fileprivate func performSearch(text: String?) {
    guard let text = text else { return }

    let separators = CharacterSet.whitespaces
    let searchTerms = text.components(separatedBy: separators)

    viewModel.searchTermsEntered(terms: searchTerms)
  }
}

extension SearchBarViewController: SearchView {
  func cancelSearch() {
    guard searchTextField.isFirstResponder else { return }

    searchTextField.resignFirstResponder()
    searchTextField.text = ""
  }

  func setSearchText(_ text: String) {
    searchTextField.text = text
  }
}

extension SearchBarViewController: UITextFieldDelegate {
  func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
    return true
  }

  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    performSearch(text: textField.text)

    return true
  }
}
