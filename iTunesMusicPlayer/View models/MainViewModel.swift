//
//  MainViewModel.swift
//  iTunesMusicPlayer
//
//  Created by Anton  Tikhonov on 4/11/17.
//  Copyright © 2017 Anton  Tikhonov. All rights reserved.
//

import Foundation

// A couterpart of TrackDataModel but fully ready for the UI.
struct TrackPresentationModel {
  let trackName: String
  let artistName: String
  let albumName: String
  let releaseDate: String
  let artworkURL: URL
  let isPlaying: Bool
}

// UI layouts we support.
enum ViewLayout {
  case tall
  case wide
}

// States of the play/pause button.
enum PlayerPlayButtonState {
  case play
  case paused
}

// Communication protocol between [view] and [view model].
// MainViewModel conforms to this protocol.
protocol ViewModel: AnyObject {
  // Attach [view] to [view model].
  // [view] is supposed to conform to ViewModelView protocol.
  func attach(with: ViewModelView)

  // Detach [view] from its [view model]
  func detach()

  // Data

  // Returns track presentation at index.
  func track(at index: Int) -> TrackPresentationModel?
  // Returns total number of tracks.
  var tracksCount: Int { get }

  // Artwork size for the list of tracks and the player view.
  var artworkSizeInList: Int { get }
  var artworkSizeInPlayer: Int { get }

  // UI logic

  // New search text entered. Each word is a separate string in the array.
  func searchTermsEntered(terms: [String])

  // A track has been tapped in the track list.
  func trackTapped(index: Int)
  // Play button tapped on music player.
  func playButtonTapped()

  // Audio player has started playing current track.
  func playerStarted()

  // Audio player has finished playing current track.
  func playerFinished()

  // Screen layout (orientation) has been changed.
  func layoutChanged(to: ViewLayout)
}

// Communication protocol between [view model] and [view]
// Used by [view model] to tell its [view] what it needs to do.
// Our [view] (actually MainViewController) conforms to this protocol.
protocol ViewModelView: AnyObject {
  // Reload list of tracks.
  func reloadTracks()
  // Select track with index in the list of tracks.
  func selectTrack(at: Int)
  // Unselect all tracks in the list of tracks.
  func unselectAllTracks()
  // Marks a track as playing/not playing
  func setTrack(at: Int, playing: Bool)

  // Cancel current search.
  func cancelSearch()

  // Bring up mini music player and load track in it.
  func activateMiniPlayerWithTrack(at: Int)
  // Dismiss mini music player.
  func deactivateMiniPlayer()
  // Activate full size music player and load track in it.
  func activateFullSizePlayerWithTrack(at: Int)
  // Dismiss full size music player.
  func deactivateFullSizePlayer()
  // Sets player's [play] button state (play/pause).
  func setPlayButton(state: PlayerPlayButtonState)

  // Change layout of the UI.
  func changeLayout(to: ViewLayout)

  // Playback control
  func playerLoad(url: URL)
  func playerPlay()
  func playerPause()

  // Display an error message
  func display(error: String)
}

class MainViewModel {
  fileprivate let datasource: DataSource
  fileprivate let dateFormatter: DateFormatter

  // MARK: - State
  fileprivate var tracks = [TrackDataModel]()
  fileprivate var searchTerms = [String]()        // list of words entered in the search field
  fileprivate var selectedTrackIndex: Int?        // index of track selected in the list of tracks, nil = none
  fileprivate var playingTrackIndex: Int?         // index of track being played, nil = none
  fileprivate var paused = false                  //
  fileprivate var viewLayout: ViewLayout = .tall  // current UI layout

  // This is our [view]
  fileprivate weak var view: ViewModelView?


  // Data source and date formatter are injected dependencies.
  // The date formatter is used to convert dates to human readable strings.
  init(datasource: DataSource, dateFormatter: DateFormatter) {
    self.datasource = datasource
    self.dateFormatter = dateFormatter
  }
}

// MARK: - ViewModel implementation
extension MainViewModel: ViewModel {

  func attach(with view: ViewModelView) {
    self.view = view
    self.datasource.start(with: self)
  }

  func detach() {
    self.view = nil
    datasource.stop()

    // wipe out the state
    tracks = []
    searchTerms = []
    selectedTrackIndex = nil
    playingTrackIndex = nil
  }

  func track(at index: Int) -> TrackPresentationModel? {
    guard tracks.indices.contains(index) else { return nil }
    let isPlaying = (index == playingTrackIndex)
    return trackPresentation(from: tracks[index], isPlaying: isPlaying)
  }

  var tracksCount: Int { return tracks.count }

  var artworkSizeInList: Int { return 60 }
  var artworkSizeInPlayer: Int { return 100 }

  func searchTermsEntered(terms: [String]) {
    guard let view = view else { fatalError() }

    self.searchTerms = terms
    datasource.loadTracksFor(artists: terms, maxTracks: 25)
    view.unselectAllTracks()
    view.deactivateMiniPlayer()
  }

  func trackTapped(index: Int) {
    guard let view = view else { fatalError() }

    selectedTrackIndex = index

    view.cancelSearch()
    view.selectTrack(at: index)

    if let playingIndex = playingTrackIndex {
      view.setPlayButton(state: playingIndex == index ? .paused : .play)
    }

    switch viewLayout {
    case .tall: view.activateMiniPlayerWithTrack(at: index)
    case .wide: view.activateFullSizePlayerWithTrack(at: index)
    }
  }

  func playButtonTapped() {
    guard let view = view else { fatalError() }

    if let playingIndex = playingTrackIndex, let selectedIndex = selectedTrackIndex {
      if playingIndex == selectedIndex {
        if paused {
          view.playerPlay()
          paused = false
          view.setPlayButton(state: .paused)
        } else {
          view.playerPause()
          paused = true
          view.setPlayButton(state: .play)
        }
      } else {
        // start playing a new track
        view.setPlayButton(state: .paused)
        view.setTrack(at: playingIndex, playing: false)
        view.setTrack(at: selectedIndex, playing: true)
        playingTrackIndex = selectedIndex
        view.playerLoad(url: tracks[selectedIndex].previewTrackURL)
        view.playerPlay()
      }
    } else if let playingIndex = playingTrackIndex {
      view.playerPause()
      view.setPlayButton(state: .play)
      view.setTrack(at: playingIndex, playing: false)
      playingTrackIndex = nil
    } else if let selectedIndex = selectedTrackIndex {
      view.setPlayButton(state: .paused)
      view.setTrack(at: selectedIndex, playing: true)
      playingTrackIndex = selectedIndex
      view.playerLoad(url: tracks[selectedIndex].previewTrackURL)
      view.playerPlay()
    }
  }

  func playerStarted() {
    // no-op for now
  }

  func playerFinished() {
    guard let view = view else { fatalError() }

    paused = false
    view.setPlayButton(state: .play)
    if let playingIndex = playingTrackIndex {
      view.setTrack(at: playingIndex, playing: false)
    }
    playingTrackIndex = nil
  }

  func layoutChanged(to newLayout: ViewLayout) {
    guard let view = view else { fatalError() }

    viewLayout = newLayout
    view.changeLayout(to: newLayout)
    switch newLayout {
    case .tall:
      view.deactivateFullSizePlayer()
      if let index = selectedTrackIndex {
        view.activateMiniPlayerWithTrack(at: index)
      }
    case .wide:
      view.deactivateMiniPlayer()
      if let index = selectedTrackIndex {
        view.activateFullSizePlayerWithTrack(at: index)
      }
    }
  }
}

// MARK: - Private helpers
fileprivate extension MainViewModel {
  func trackPresentation(from model: TrackDataModel, isPlaying: Bool) -> TrackPresentationModel {
    return TrackPresentationModel(trackName: model.trackName,
                                  artistName: model.artistName,
                                  albumName: model.albumName ?? "Unknown Album",
                                  releaseDate: model.releaseDate != nil ? dateFormatter.string(from: model.releaseDate!) : "",
                                  artworkURL: model.artworkURL ?? artworkPlaceholderURL(),
                                  isPlaying: isPlaying)
  }

  func artworkPlaceholderURL() -> URL {
    let bundle = Bundle.main
    let name = "898-music-note-toolbar"
    let ext = "png"
    guard
      let url = bundle.url(forResource: name, withExtension: ext)
    else { fatalError("can't find resource file \(name).\(ext)") }
    return url
  }
}

// MARK: - Data source delegate
extension MainViewModel: DataSourceDelegate {
  func added(tracks: [TrackDataModel]) {
    assert(Thread.isMainThread)
    guard let view = view else { fatalError() }

    self.tracks = tracks
    view.reloadTracks()
  }

  func errorOccured(error: String?) {
    // TODO: needs implementation
    assert(Thread.isMainThread)
  }
}
