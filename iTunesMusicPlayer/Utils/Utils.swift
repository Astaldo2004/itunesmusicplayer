//
//  Utils.swift
//  iTunesMusicPlayer
//
//  Created by Anton  Tikhonov on 4/11/17.
//  Copyright © 2017 Anton  Tikhonov. All rights reserved.
//

import Foundation

// Random collection of things I like to use but don't have a good place to put to.

// Allows to provide a string as a default value for an optional of any type.
// Very useful for string interpolation of optionals.
// Example:
//     func workFinished(error: Error?) {
//        .....
//        print("finished, \(error ??? "no error")")
//        .....
//     }
// Here we provide a string "no error" as a default value for an optional of type Error?,
// which is not possible with the regular ?? operator.
// The @autoclosure construct makes sure the defaultValue is only evaluated if the optional is nil,
// so you don't have performance penalty even if the defaultValue is an interpolated string.
infix operator ???: NilCoalescingPrecedence

public func ??? <T>(optional: T?, defaultValue: @autoclosure () -> String) -> String {
  return optional.map { String(describing: $0) } ?? defaultValue()
}

// Runs block on the main thread.
// If we're already on the main thread, we run the block synchronously.
// If not, we schedule it on the main thread asynchrounously.
// Using this function instead of DispatchQueue.main.async is very useful for unit-testing
func onMainThread(_ block: @escaping () -> Void) {
  if Thread.isMainThread {
    block()
  } else {
    DispatchQueue.main.async {
      block()
    }
  }
}
