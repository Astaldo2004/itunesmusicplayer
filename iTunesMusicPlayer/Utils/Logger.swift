//
//  Logger.swift
//  iTunesMusicPlayer
//
//  Created by Anton  Tikhonov on 4/11/17.
//  Copyright © 2017 Anton  Tikhonov. All rights reserved.
//

import Foundation

// Very primitive logger API.
// I prefer NSLog over Swift's print because it provides timing and current thread infos.

func log(_ msg: String) {
  NSLog("%@", msg)
}

func logError(_ msg: String) {
  NSLog("[E] %@", msg)
}
