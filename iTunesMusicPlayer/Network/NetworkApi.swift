//
//  NetworkApi.swift
//  iTunesMusicPlayer
//
//  Created by Anton  Tikhonov on 4/11/17.
//  Copyright © 2017 Anton  Tikhonov. All rights reserved.
//

import UIKit

fileprivate let kFetchTrackBaseURL = "https://itunes.apple.com/search?media=music&entity=song&attribute=artistTerm"

enum FetchResult<T> {
  case success(T)
  case failed(String?)
}

protocol NetworkApi: AnyObject {
  typealias FetchTracksCompletionHandler = (FetchResult<Data>) -> Void
  func fetchTracks(artists: [String], maxTracks: Int, completion: @escaping FetchTracksCompletionHandler)

  typealias FetchArtworkCompletionHandler = (FetchResult<UIImage>) -> Void
  func fetchArtwork(url: URL, completion: @escaping FetchArtworkCompletionHandler)

  func cancelOutstandingRequests()
}

class ItunesNetworkApi: NetworkApi {

  static var shared = ItunesNetworkApi()

  fileprivate lazy var session: URLSession = {
    let configuration = URLSessionConfiguration.default
    // TODO: here we can change our session configuration, if need ever need to.
    return URLSession(configuration: configuration)
  }()

  // MARK: - NetworkApi implementation
  func fetchArtwork(url: URL, completion: @escaping (FetchResult<UIImage>) -> Void) {
    session.dataTask(with: url) { (data, response, clientSideError) in
      assert(!Thread.isMainThread)

      let errorDescription = ItunesNetworkApi.check(response: response, error: clientSideError)
      guard errorDescription == nil else {
        completion(.failed(errorDescription))
        return
      }

      guard let data = data, !data.isEmpty else {
        completion(.failed("No data"))
        return
      }

      guard let image = UIImage(data: data) else {
        completion(.failed("Wrong image data"))
        return
      }

      completion(.success(image))
    }.resume()
  }

  func fetchTracks(artists: [String], maxTracks: Int, completion: @escaping (FetchResult<Data>) -> Void) {
    // If params are incorrect, this is our error. To help with debugging, we crash immediately.
    // TODO: In release we would probably want to log this error and bail off gracefully.
    guard artists.count != 0, 1...2000 ~= maxTracks else { fatalError("Wrong parameters") }
    guard let url = fetchTracksURL(term: artists, limit: maxTracks) else { fatalError("Wrong URL") }

    session.dataTask(with: url) { (data, response, clientSideError) in
      assert(!Thread.isMainThread)

      let errorDescription = ItunesNetworkApi.check(response: response, error: clientSideError)
      guard errorDescription == nil else {
        completion(.failed(errorDescription))
        return
      }

      guard let data = data, !data.isEmpty else {
        completion(.failed("No data"))
        return
      }

      completion(.success(data))
    }.resume()
  }

  func cancelOutstandingRequests() {
    // TODO: needs impelementation
  }
}

// MARK: - Private helpers
fileprivate extension ItunesNetworkApi {
  // Checks server response and cliend side error.
  // Returns error description as string, or nil if no error.
  static func check(response: URLResponse?, error: Error?) -> String? {
    // If we have client side error, just return it.
    guard error == nil else { return error?.localizedDescription }

    // Check server response.
    if let http = response as? HTTPURLResponse {
      guard 200..<300 ~= http.statusCode else { return "Wrong http status \(http.statusCode)" }
    }

    return nil // no error
  }

  func fetchTracksURL(term: [String], limit: Int) -> URL? {
    let urlStr = "\(kFetchTrackBaseURL)&term=\(term.joined(separator: "+"))&limit=\(limit)"
    return URL(string: urlStr)
  }
}
