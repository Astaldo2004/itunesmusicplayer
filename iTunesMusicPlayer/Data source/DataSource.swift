//
//  DataSource.swift
//  iTunesMusicPlayer
//
//  Created by Anton  Tikhonov on 4/11/17.
//  Copyright © 2017 Anton  Tikhonov. All rights reserved.
//

import Foundation

// Data source delegate. Called on the main thread.
protocol DataSourceDelegate: AnyObject {
  func added(tracks: [TrackDataModel])
  func errorOccured(error: String?)
}

// Data source public contol API
protocol DataSource: AnyObject {
  func start(with: DataSourceDelegate)
  func stop()
  func loadTracksFor(artists: [String], maxTracks: Int)
}

final class ItunesDataSource: DataSource {

  fileprivate weak var delegate: DataSourceDelegate?
  fileprivate var networkApi: NetworkApi

  // Network API is passed as a depencency injection.
  init(networkApi: NetworkApi) {
    self.networkApi = networkApi
  }

  func start(with delegate: DataSourceDelegate) {
    self.delegate = delegate
  }

  func stop() {
    self.delegate = nil
    networkApi.cancelOutstandingRequests()
  }

  func loadTracksFor(artists: [String], maxTracks: Int) {
    networkApi.fetchTracks(artists: artists, maxTracks: maxTracks) { [weak self] result in
      guard let sself = self else { return }

      switch result {
      case .success(let data):
        sself.handle(data: data)
      case .failed(let errorDescription):
        sself.report(error: errorDescription)
      }
    }
  }
}

// MARK: - Private helpers
fileprivate extension ItunesDataSource {
  func handle(data: Data) {
    guard
      let mayBeJson = try? JSONSerialization.jsonObject(with: data, options: .allowFragments),
      let json = mayBeJson as? [String : Any],
      let results = json["results"] as? [[String : Any]]
    else {
      report(error: "Error parsing response")
      return
    }

    // JSON conversion occures on a background thread.
    // After it's complete, we jump to the main thread to report to our delegate.
    let tracks = results.flatMap { TrackDataModel(json: $0) }
    onMainThread {
      self.delegate?.added(tracks: tracks)
    }
  }

  func report(error: String?) {
    onMainThread {
      self.delegate?.errorOccured(error: error)
    }
  }
}
