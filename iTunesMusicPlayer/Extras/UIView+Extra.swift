//
//  UIView+Extra.swift
//  iTunesMusicPlayer
//
//  Created by Anton  Tikhonov on 4/11/17.
//  Copyright © 2017 Anton  Tikhonov. All rights reserved.
//

import UIKit

extension UIView {
  // Simplifies creation of views intended for autolayout.
  // Examples:
  //    let view = UIView(frame: .zero).forAutolayout()
  //    let button = UIButton(type: .system).forAutolayout()
  public func forAutolayout() -> Self {
    translatesAutoresizingMaskIntoConstraints = false
    return self
  }
}
