//
//  UIViewController+Extras.swift
//  iTunesMusicPlayer
//
//  Created by Anton  Tikhonov on 6/11/17.
//  Copyright © 2017 Anton  Tikhonov. All rights reserved.
//

import UIKit

extension UIViewController {
  func attachChild(viewController: UIViewController, block: () -> Void) {
    addChildViewController(viewController)
    viewController.view.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(viewController.view)

    block()

    viewController.didMove(toParentViewController: self)
  }

  func detachChild(viewController: UIViewController) {
    viewController.view.removeFromSuperview()
    viewController.removeFromParentViewController()
  }
}
