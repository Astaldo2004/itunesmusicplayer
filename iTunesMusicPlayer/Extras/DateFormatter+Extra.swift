//
//  DateFormatter+Extra.swift
//  iTunesMusicPlayer
//
//  Created by Anton  Tikhonov on 4/11/17.
//  Copyright © 2017 Anton  Tikhonov. All rights reserved.
//

import Foundation

extension DateFormatter {
  // iTunes json dates are coming in this form: "2015-08-21T07:00:00Z"
  // This is definitely some ISO standard format, but I can't remember which one.
  static var itunes: DateFormatter {
    let df = DateFormatter()
    // itunes dates are always localized in Engligh
    df.locale = Locale(identifier: "en_US_POSIX")
    df.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    return df
  }

  // It's a reference date formatter suitable for checking dates in tests.
  // It converts dates to this format: "Nov 4 2017 12:58 PM"
  static var reference: DateFormatter {
    let df = DateFormatter()
    // Lock locale and time zone to get same values when coverting dates to strings
    // regardless of current location and time zone.
    df.locale = Locale(identifier: "en_US_POSIX")
    df.timeZone = TimeZone(secondsFromGMT: 0)
    df.dateFormat = "MMM d yyyy h:mm a"
    return df
  }
}
