//
//  String+Extra.swift
//  iTunesMusicPlayer
//
//  Created by Anton  Tikhonov on 4/11/17.
//  Copyright © 2017 Anton  Tikhonov. All rights reserved.
//

import Foundation

extension String {
  func quoted() -> String { return "\"\(self)\"" }
}
