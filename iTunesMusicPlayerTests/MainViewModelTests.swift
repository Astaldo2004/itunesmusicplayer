//
//  MainViewModelTests.swift
//  iTunesMusicPlayerTests
//
//  Created by Anton  Tikhonov on 6/11/17.
//  Copyright © 2017 Anton  Tikhonov. All rights reserved.
//

import XCTest

class MainViewModelTests: XCTestCase {

  func test_itCanHandleEmptySearchResults() {
    let datasource = TestDataSource(numberOfTracks: 0)
    let view = TestView()
    let viewModel = MainViewModel(datasource: datasource, dateFormatter: DateFormatter.reference)
    viewModel.attach(with: view)
    viewModel.searchTermsEntered(terms: ["Vampire Weekend"])
    viewModel.detach()

    let reference = ["reloadTracks", "unselectAllTracks", "deactivateMiniPlayer"]
    XCTAssertEqual(view.opLog, reference)
  }

  func test_itCanHandleNonEmptySearchResults() {
    let datasource = TestDataSource(numberOfTracks: 10)
    let view = TestView()
    let viewModel = MainViewModel(datasource: datasource, dateFormatter: DateFormatter.reference)
    viewModel.attach(with: view)
    viewModel.searchTermsEntered(terms: ["The Beatles"])
    viewModel.trackTapped(index: 5)
    viewModel.playButtonTapped()
    viewModel.playerStarted()
    viewModel.playerFinished()
    viewModel.detach()


    let reference = ["reloadTracks", "unselectAllTracks", "deactivateMiniPlayer", "cancelSearch",
                     "selectTrack 5", "activateMiniPlayerWithTrack 5", "setPlayButton paused", "setTrack 5 true",
                     "playerLoad https://previewurl.com/5.mp4a", "playerPlay", "setPlayButton play", "setTrack 5 false"]
    XCTAssertEqual(view.opLog, reference)
  }

  func test_itCanHandleOrientationChange() {
    let datasource = TestDataSource(numberOfTracks: 10)
    let view = TestView()
    let viewModel = MainViewModel(datasource: datasource, dateFormatter: DateFormatter.reference)
    viewModel.attach(with: view)
    viewModel.layoutChanged(to: .tall)
    viewModel.layoutChanged(to: .wide)
    viewModel.detach()

    let reference = ["changeLayout tall", "deactivateFullSizePlayer", "changeLayout wide", "deactivateMiniPlayer"]
    XCTAssertEqual(view.opLog, reference)
  }
}

final class TestDataSource: DataSource {
  weak var delegate: DataSourceDelegate?
  private let numberOfTracks: Int

  init(numberOfTracks: Int) {
    self.numberOfTracks = numberOfTracks
  }

  func start(with delegate: DataSourceDelegate) {
    self.delegate = delegate
  }

  func stop() {
    self.delegate = nil
  }

  func loadTracksFor(artists: [String], maxTracks: Int) {
    let tracks = generateTracks(count: numberOfTracks)
    delegate?.added(tracks: tracks)
  }

  private func generateTracks(count: Int) -> [TrackDataModel] {
    return (0..<count).map { index in
      let json: [String : Any] = [
        "trackId" : index,
        "artistName" : "Artist",
        "collectionName" : "Album Name \(index)",
        "trackName" : "Track Name \(index)",
        "previewUrl" : "https://previewurl.com/\(index).mp4a",
        "artworkUrl30" : "http://artworkurl.com/\(index)/30x30bb.jpg",
        "artworkUrl60" : "http://artworkurl.com/\(index)/60x60bb.jpg",
        "artworkUrl100" : "http://artworkurl.com/\(index)/100x100bb.jpg",
        "releaseDate" : "2015-08-21T07:00:00Z",
        "trackTimeMillis" : 286000]
      return TrackDataModel(json: json)!
    }
  }
}

// Works as a recorder of operations.
// In the end we get an array of strings describing what our [model view] was doing.
// We just need to compare it to a reference one to decide if the [model view] did well.
final class TestView: ViewModelView {

  var opLog = [String]()
  weak var viewModel: ViewModel?

  func reloadTracks() {
    opLog.append("reloadTracks")
  }

  func selectTrack(at: Int) {
    opLog.append("selectTrack \(at)")
  }

  func unselectAllTracks() {
    opLog.append("unselectAllTracks")
  }

  func setTrack(at index: Int, playing: Bool) {
    opLog.append("setTrack \(index) \(playing)")
  }

  func cancelSearch() {
    opLog.append("cancelSearch")
  }

  func activateMiniPlayerWithTrack(at index: Int) {
    opLog.append("activateMiniPlayerWithTrack \(index)")
  }

  func deactivateMiniPlayer() {
    opLog.append("deactivateMiniPlayer")
  }

  func activateFullSizePlayerWithTrack(at index: Int) {
    opLog.append("activateFullSizePlayerWithTrack \(index)")
  }

  func deactivateFullSizePlayer() {
    opLog.append("deactivateFullSizePlayer")
  }

  func setPlayButton(state: PlayerPlayButtonState) {
    let stateName: String = {
      switch state {
      case .play: return "play"
      case .paused: return "paused"
      }
    }()
    opLog.append("setPlayButton \(stateName)")
  }

  func changeLayout(to layout: ViewLayout) {
    let layoutName: String = {
      switch layout {
      case .tall: return "tall"
      case .wide: return "wide"
      }
    }()
    opLog.append("changeLayout \(layoutName)")
  }

  func playerLoad(url: URL) {
    opLog.append("playerLoad \(url)")
  }

  func playerPlay() {
    opLog.append("playerPlay")
  }

  func playerPause() {
    opLog.append("playerPause")
  }

  func display(error: String) {
    opLog.append("displayError \(error)")
  }
}
