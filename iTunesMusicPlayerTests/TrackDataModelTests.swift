//
//  TrackDataModelTests.swift
//  iTunesMusicPlayerTests
//
//  Created by Anton  Tikhonov on 4/11/17.
//  Copyright © 2017 Anton  Tikhonov. All rights reserved.
//

import XCTest

class TrackDataModelTests: XCTestCase {

  override func setUp() {
    super.setUp()
    // Put setup code here. This method is called before the invocation of each test method in the class.
  }

  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }

  // Makes sure TrackDataModel can be created from good data.
  func test_itCanBeCreated() {
    let json: [String : Any] = [
      "trackId" : 1006937456,
      "artistName" : "Artist",
      "collectionName" : "Album Name",
      "trackName" : "Track Name",
      "previewUrl" : "https://previewurl.com",
      "artworkUrl30" : "http://artworkurl.com/30x30bb.jpg",
      "artworkUrl60" : "http://artworkurl.com/60x60bb.jpg",
      "artworkUrl100" : "http://artworkurl.com/100x100bb.jpg",
      "releaseDate" : "2015-08-21T07:00:00Z",
      "trackTimeMillis" : 286000]

    guard let model = TrackDataModel(json: json) else { XCTFail(); return }

    XCTAssertEqual(model.artistName, "Artist")
    XCTAssertEqual(model.trackID, 1006937456)
    XCTAssertEqual(model.trackName, "Track Name")
    XCTAssertEqual(model.previewTrackURL, URL(string: "https://previewurl.com"))
    XCTAssertEqual(model.albumName, "Album Name")
    XCTAssertEqual(model.artworkURL, URL(string: "http://artworkurl.com/60x60bb.jpg"))
    if let releaseDate = model.releaseDate {
      XCTAssertEqual(DateFormatter.reference.string(from: releaseDate), "Aug 21 2015 7:00 AM")
    } else {
      XCTFail("Release date is missing")
    }
    if let trackLength = model.trackLengthSec {
      XCTAssertEqual(trackLength, TimeInterval(286))
    } else {
      XCTFail("Track length is missing")
    }
  }

  func test_itCanHandleBadData() {
    // TODO: needs implementation
  }
}
