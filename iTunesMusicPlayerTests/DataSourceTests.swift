//
//  iTunesMusicPlayerTests.swift
//  iTunesMusicPlayerTests
//
//  Created by Anton  Tikhonov on 4/11/17.
//  Copyright © 2017 Anton  Tikhonov. All rights reserved.
//

import XCTest

class DataSourceTests: XCTestCase {

  func test_itCanLoadData() {
    let delegate = TestDataSourceDelegate()
    let network = SuccessfulNetworkApi()
    let datasource = ItunesDataSource(networkApi: network)

    datasource.start(with: delegate)
    datasource.loadTracksFor(artists: ["Does not matter"], maxTracks: 25)
    datasource.stop()

    // Make sure we have read 25 tracks, there were no erors and
    // cancelOutstandingRequests() has been called by data source.
    XCTAssertEqual(delegate.tracks.count, 25)
    XCTAssert(delegate.errors.isEmpty)
    XCTAssertEqual(network.cancelOutstandingRequestsCallCount, 1)
  }

  func test_itCanHandleErrors() {
    let delegate = TestDataSourceDelegate()
    let network = AlwaysFailingNetworkApi()
    let datasource = ItunesDataSource(networkApi: network)

    datasource.start(with: delegate)
    datasource.loadTracksFor(artists: ["Does not matter"], maxTracks: 25)
    datasource.stop()

    // Make sure we have read 25 tracks, there were no erors and
    // cancelOutstandingRequests() has been called by data source.
    XCTAssertEqual(delegate.tracks.count, 0)
    XCTAssertEqual(delegate.errors, ["Error occured"])
    XCTAssertEqual(network.cancelOutstandingRequestsCallCount, 1)
  }

  fileprivate final class TestDataSourceDelegate: DataSourceDelegate {
    var tracks = [TrackDataModel]()
    var errors = [String]()

    func added(tracks: [TrackDataModel]) {
      self.tracks = tracks
    }

    func errorOccured(error: String?) {
      self.errors.append(error ?? "")
    }
  }

  fileprivate final class SuccessfulNetworkApi: NetworkApi {
    var cancelOutstandingRequestsCallCount = 0

    func fetchTracks(artists: [String], maxTracks: Int, completion: @escaping FetchTracksCompletionHandler) {
      let data = loadDataFixture(name: "DataSourceTests", ext: "json")
      completion(.success(data))
    }

    func fetchArtwork(url: URL, completion: @escaping FetchArtworkCompletionHandler) {
      // We don't test artwork fetching here.
    }

    func cancelOutstandingRequests() {
      cancelOutstandingRequestsCallCount += 1
    }

    private func loadDataFixture(name: String, ext: String) -> Data {
      let testBundle = Bundle(for: type(of: self))
      guard let url = testBundle.url(forResource: name, withExtension: ext) else { fatalError("can't find data file \(name).\(ext)") }
      guard let data = try? Data(contentsOf: url) else { fatalError("can't load data from file \(name).\(ext)") }
      return data
    }
  }

  fileprivate final class AlwaysFailingNetworkApi: NetworkApi {
    var cancelOutstandingRequestsCallCount = 0

    func fetchTracks(artists: [String], maxTracks: Int, completion: @escaping FetchTracksCompletionHandler) {
      completion(.failed("Error occured"))
    }

    func fetchArtwork(url: URL, completion: @escaping FetchArtworkCompletionHandler) {
      completion(.failed("Error occured"))
    }

    func cancelOutstandingRequests() {
      cancelOutstandingRequestsCallCount += 1
    }
  }
}

